/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.gauge;

import eu.hansolo.enzo.common.Section;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.IntegerPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;


/**
 * User: hansolo
 * Date: 29.11.13
 * Time: 13:40
 */
public class Gauge extends Region {
    private static final double PREFERRED_WIDTH  = 200;
    private static final double PREFERRED_HEIGHT = 200;
    private static final double MINIMUM_WIDTH    = 50;
    private static final double MINIMUM_HEIGHT   = 50;
    private static final double MAXIMUM_WIDTH    = 1024;
    private static final double MAXIMUM_HEIGHT   = 1024;
    private double          size;
    private Pane            pane;
    private Canvas          sectionsCanvas;
    private GraphicsContext sectionsCtx;
    private Canvas          measuredRangeCanvas;
    private GraphicsContext measuredRangeCtx;
    private Path            needle;
    private Rotate          needleRotate;
    private Text            valueText;
    private Text            titleText;
    private double          angleStep;
    private Timeline        timeline;
    private boolean         isFirstTime;

    // Control related    
    private static final Color DEFAULT_VALUE_TEXT_COLOR   = Color.web("#ffffff");
    private static final Color DEFAULT_TITLE_TEXT_COLOR   = Color.web("#ffffff");
    private static final Color DEFAULT_SECTION_TEXT_COLOR = Color.web("#ffffff");
    private static final Color DEFAULT_RANGE_FILL         = Color.rgb(0, 0, 0, 0.25);
    private DoubleProperty          value;
    private double                  oldValue;
    private DoubleProperty          minValue;
    private double                  exactMinValue;
    private DoubleProperty          maxValue;
    private double                  exactMaxValue;
    private double                  minMeasuredValue;
    private double                  maxMeasuredValue;
    private int                     _decimals;
    private IntegerProperty         decimals;
    private String                  _unit;
    private StringProperty          unit;
    private boolean                 _animated;
    private BooleanProperty         animated;
    private double                  animationDuration;
    private double                  _startAngle;
    private DoubleProperty          startAngle;
    private double                  _angleRange;
    private DoubleProperty          angleRange;
    private boolean                 _clockwise;
    private BooleanProperty         clockwise;
    private boolean                 _autoScale;
    private BooleanProperty         autoScale;
    private boolean                 _sectionTextVisible;
    private BooleanProperty         sectionTextVisible;
    private boolean                 _sectionIconVisible;
    private BooleanProperty         sectionIconVisible;
    private boolean                 _measuredRangeVisible;
    private BooleanProperty         measuredRangeVisible;
    private Color                   _needleColor;
    private ObjectProperty<Color>   needleColor;
    private ObservableList<Section> sections;
    private double                  _majorTickSpace;
    private DoubleProperty          majorTickSpace;
    private double                  _minorTickSpace;
    private DoubleProperty          minorTickSpace;
    private String                  _title;
    private StringProperty          title;
    private ObjectProperty<Color>   valueTextColor;
    private ObjectProperty<Color>   titleTextColor;
    private ObjectProperty<Color>   sectionTextColor;
    private ObjectProperty<Color>   rangeFill;


    // ******************** Constructors **************************************
    public Gauge() {
        getStylesheets().add(Gauge.class.getResource("simple-gauge.css").toExternalForm());
        getStyleClass().add("simple-gauge");

        value = new DoublePropertyBase(0) {
            @Override protected void invalidated() {
                set(clamp(getMinValue(), getMaxValue(), get()));
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "value"; }
        };
        minValue = new DoublePropertyBase(0) {
            @Override protected void invalidated() {
                set(clamp(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, get()));
                if (getValue() < get()) setValue(get());
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "minValue"; }
        };
        maxValue = new DoublePropertyBase(100) {
            @Override protected void invalidated() {
                set(clamp(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, get()));
                if (getValue() > get()) setValue(get());
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "maxValue"; }
        };
        oldValue = 0;
        _decimals = 0;
        _unit = "";
        _animated = true;
        _startAngle = 315;
        _angleRange = 270;
        _clockwise = true;
        _autoScale = false;
        _needleColor = Color.web("#5a615f");
        _sectionTextVisible = false;
        _sectionIconVisible = false;
        _measuredRangeVisible = false;
        sections = FXCollections.observableArrayList();
        _majorTickSpace = 10;
        _minorTickSpace = 1;
        _title = "";
        animationDuration     = 3000;

        angleStep = getAngleRange() / (getMaxValue() - getMinValue());
        timeline = new Timeline();

        init();
        initGraphics();
        registerListeners();
        isFirstTime = true;
    }


    // ******************** Initialization ************************************
    private void init() {
        if (Double.compare(getWidth(), 0) <= 0 ||
            Double.compare(getHeight(), 0) <= 0 ||
            Double.compare(getPrefWidth(), 0) <= 0 ||
            Double.compare(getPrefHeight(), 0) <= 0) {
            setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        }
        if (Double.compare(getMinWidth(), 0) <= 0 ||
            Double.compare(getMinHeight(), 0) <= 0) {
            setMinSize(MINIMUM_HEIGHT, MINIMUM_HEIGHT);
        }
        if (Double.compare(getMaxWidth(), 0) <= 0 ||
            Double.compare(getMaxHeight(), 0) <= 0) {
            setMaxSize(MAXIMUM_HEIGHT, MAXIMUM_HEIGHT);
        }
    }

    private void initGraphics() {
        Font.loadFont(getClass().getResourceAsStream("/eu/hansolo/enzo/common/opensans-semibold.ttf"), (0.06 * PREFERRED_HEIGHT)); // "OpenSans"

        sectionsCanvas = new Canvas(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        sectionsCtx    = sectionsCanvas.getGraphicsContext2D();

        measuredRangeCanvas = new Canvas(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        measuredRangeCanvas.setManaged(isMeasuredRangeVisible());
        measuredRangeCanvas.setVisible(isMeasuredRangeVisible());
        measuredRangeCtx    = measuredRangeCanvas.getGraphicsContext2D();

        if (getValue() < getMinValue()) setValue(getMinValue());
        if (getValue() > getMaxValue()) setValue(getMaxValue());

        needleRotate = new Rotate(180 - getStartAngle());
        if (getMinValue() < 0) {
            needleRotate.setAngle(needleRotate.getAngle() + (getValue() - getOldValue() - getMinValue()) * angleStep);
        } else {
            //needleRotate.setAngle(needleRotate.getAngle() + (getValue() - getOldValue() + getMinValue()) * angleStep);
        }

        angleStep = getAngleRange() / (getMaxValue() - getMinValue());
        needleRotate.setAngle(needleRotate.getAngle() + (getValue() - getOldValue()) * angleStep);

        needle = new Path();
        needle.setFillRule(FillRule.EVEN_ODD);
        needle.getStyleClass().setAll("needle");
        needle.getTransforms().setAll(needleRotate);

        valueText = new Text(String.format(Locale.US, "%." + getDecimals() + "f", getMinValue()) + getUnit());
        valueText.setMouseTransparent(true);
        valueText.setTextOrigin(VPos.CENTER);
        valueText.getStyleClass().setAll("value");

        titleText = new Text(getTitle());
        titleText.setTextOrigin(VPos.CENTER);
        titleText.getStyleClass().setAll("title");

        // Add all nodes
        pane = new Pane();
        pane.getStyleClass().add("simple-gauge");
        pane.getChildren().setAll(sectionsCanvas,
                                  measuredRangeCanvas,
                                  needle,
                                  valueText,
                                  titleText);

        getChildren().setAll(pane);
        resize();
    }

    private void registerListeners() {
        widthProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        heightProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        valueProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("VALUE");}});
        minValueProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RECALC");}});
        maxValueProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RECALC");}});
        titleProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        needleColorProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("NEEDLE_COLOR");}});
        animatedProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("ANIMATED");}});
        angleRangeProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("ANGLE_RANGE");}});
        sectionTextVisibleProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        sectionIconVisibleProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        valueTextColorProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        titleTextColorProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        sectionTextColorProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("RESIZE");}});
        measuredRangeVisibleProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("MEASURED_RANGE_VISIBLE");}});
        getSections().addListener(new ListChangeListener<Section>() {
            @Override public void onChanged(Change<? extends Section> change) {handleControlPropertyChanged("RESIZE");
            }
        });

        needleRotate.angleProperty().addListener(new InvalidationListener() {@Override public void invalidated(Observable observable) {handleControlPropertyChanged("ANGLE");}});

        // Avoid set the first value as minMeasuredValue
        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent actionEvent) {
                if (isFirstTime) {
                    isFirstTime = false;
                    setMinMeasuredValue(getValue());
                    setMaxMeasuredValue(getValue());
                }
            }
        });
    }


    // ******************** Methods *******************************************
    protected void handleControlPropertyChanged(final String PROPERTY) {
        if ("RESIZE".equals(PROPERTY)) {
            resize();
        } else if ("VALUE".equals(PROPERTY)) {
            rotateNeedle();
        } else if ("RECALC".equals(PROPERTY)) {
            if (getMinValue() < 0) {
                angleStep = getAngleRange() / (getMaxValue() - getMinValue());
                needleRotate.setAngle(180 - getStartAngle() - (getMinValue()) * angleStep);
            } else {
                angleStep = getAngleRange() / (getMaxValue() + getMinValue());
                needleRotate.setAngle(180 - getStartAngle() * angleStep);
            }
            resize();
        } else if ("ANGLE".equals(PROPERTY)) {
            double currentValue = (needleRotate.getAngle() + getStartAngle() - 180) / angleStep + getMinValue();
            valueText.setText(String.format(Locale.US, "%." + getDecimals() + "f", currentValue) + getUnit());
            valueText.setTranslateX((size - valueText.getLayoutBounds().getWidth()) * 0.5);
            if (valueText.getLayoutBounds().getWidth() > 0.45 * size) {
                resizeText();
            }
            // Check sections
            for (Section section : getSections()) {
                if (section.contains(currentValue)) {
                    section.fireSectionEvent(new Section.SectionEvent(section, null, Section.SectionEvent.ENTERING_SECTION));
                    break;
                }
            }
            // Adjust minMeasured and maxMeasured values after value was set for the first time
            if (isFirstTime) return;
            if (currentValue < getMinMeasuredValue()) {
                setMinMeasuredValue(currentValue);
            }
            if (currentValue > getMaxMeasuredValue()) {
                setMaxMeasuredValue(currentValue);
            }
            if (isMeasuredRangeVisible()) drawMeasuredRange();
        } else if ("MEASURED_RANGE_VISIBLE".equals(PROPERTY)) {
            measuredRangeCanvas.setManaged(isMeasuredRangeVisible());
            measuredRangeCanvas.setVisible(isMeasuredRangeVisible());
        } else if ("NEEDLE_COLOR".equals(PROPERTY)) {
            resize();
        }
    }

    public void resetNeedle() {
        timeline.stop();
        boolean wasAnimated = isAnimated();
        setAnimated(false);
        setValue(getMinValue());
        if (wasAnimated) {
            setAnimated(true);
        }
    }

    @Override protected double computeMinWidth(final double HEIGHT) {
        return super.computeMinWidth(Math.max(MINIMUM_HEIGHT, HEIGHT - getInsets().getTop() - getInsets().getBottom()));
    }
    @Override protected double computeMinHeight(final double WIDTH) {
        return super.computeMinHeight(Math.max(MINIMUM_WIDTH, WIDTH - getInsets().getLeft() - getInsets().getRight()));
    }

    @Override protected double computeMaxWidth(final double HEIGHT) {
        return super.computeMaxWidth(Math.min(MAXIMUM_HEIGHT, HEIGHT - getInsets().getTop() - getInsets().getBottom()));
    }
    @Override protected double computeMaxHeight(final double WIDTH) {
        return super.computeMaxHeight(Math.min(MAXIMUM_WIDTH, WIDTH - getInsets().getLeft() - getInsets().getRight()));
    }

    @Override protected double computePrefWidth(final double HEIGHT) {
        double prefHeight = PREFERRED_HEIGHT;
        if (HEIGHT != -1) {
            prefHeight = Math.max(0, HEIGHT - getInsets().getTop() - getInsets().getBottom());
        }
        return super.computePrefWidth(prefHeight);
    }
    @Override protected double computePrefHeight(final double WIDTH) {
        double prefWidth = PREFERRED_WIDTH;
        if (WIDTH != -1) {
            prefWidth = Math.max(0, WIDTH - getInsets().getLeft() - getInsets().getRight());
        }
        return super.computePrefHeight(prefWidth);
    }

    public final double getValue() {
        return value.get();
    }
    public final void setValue(final double VALUE) {
        oldValue = valueProperty().get();
        value.set(VALUE);
    }
    public final DoubleProperty valueProperty() {
        return value;
    }

    public final double getOldValue() {
        return oldValue;
    }

    public final double getMinValue() {
        return minValue.get();
    }
    public final void setMinValue(final double MIN_VALUE) {
        minValue.set(MIN_VALUE);
    }
    public final DoubleProperty minValueProperty() {
        return minValue;
    }

    public final double getMaxValue() {
        return maxValue.get();
    }
    public final void setMaxValue(final double MAX_VALUE) {
        maxValue.set(MAX_VALUE);
    }
    public final DoubleProperty maxValueProperty() {
        return maxValue;
    }

    public final double getMinMeasuredValue() {
        return minMeasuredValue;
    }
    public final void setMinMeasuredValue(final double MIN_MEASURED_VALUE) {
        minMeasuredValue = clamp(getMinValue(), getMaxValue(), MIN_MEASURED_VALUE);
    }

    public final double getMaxMeasuredValue() {
        return maxMeasuredValue;
    }
    public final void setMaxMeasuredValue(final double MAX_MEASURED_VALUE) {
        maxMeasuredValue = clamp(getMinValue(), getMaxValue(), MAX_MEASURED_VALUE);
    }

    public final void resetMinMaxMeasuredValues() {
        minMeasuredValue = getValue();
        maxMeasuredValue = getValue();
    }

    public final int getDecimals() {
        return null == decimals ? _decimals : decimals.get();
    }
    public final void setDecimals(final int DECIMALS) {
        if (null == decimals) {
            _decimals = clamp(0, 3, DECIMALS);
        } else {
            decimals.set(DECIMALS);
        }
    }
    public final IntegerProperty decimalsProperty() {
        if (null == decimals) {
            decimals  = new IntegerPropertyBase(_decimals) {
                @Override protected void invalidated() { set(clamp(0, 3, get())); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "decimals"; }
            };
        }
        return decimals;
    }

    public final String getUnit() {
        return null == unit ? _unit : unit.get();
    }
    public final void setUnit(final String UNIT) {
        if (null == unit) {
            _unit = UNIT;
        } else {
            unit.set(UNIT);
        }
    }
    public final StringProperty unitProperty() {
        if (null == unit) {
            unit = new SimpleStringProperty(this, "unit", _unit);
        }
        return unit;
    }

    public final boolean isAnimated() {
        return null == animated ? _animated : animated.get();
    }
    public final void setAnimated(final boolean ANIMATED) {
        if (null == animated) {
            _animated = ANIMATED;
        } else {
            animated.set(ANIMATED);
        }
    }
    public final BooleanProperty animatedProperty() {
        if (null == animated) {
            animated = new SimpleBooleanProperty(this, "animated", _animated);
        }
        return animated;
    }

    public double getStartAngle() {
        return null == startAngle ? _startAngle : startAngle.get();
    }
    public final void setStartAngle(final double START_ANGLE) {
        if (null == startAngle) {
            _startAngle = clamp(0, 360, START_ANGLE);
        } else {
            startAngle.set(START_ANGLE);
        }
    }
    public final DoubleProperty startAngleProperty() {
        if (null == startAngle) {
            startAngle = new DoublePropertyBase(_startAngle) {
                @Override protected void invalidated() {
                    set(clamp(0d, 360d, get()));
                }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "startAngle"; }
            };
        }
        return startAngle;
    }

    public final double getAnimationDuration() {
        return animationDuration;
    }
    public final void setAnimationDuration(final double ANIMATION_DURATION) {
        animationDuration = clamp(20, 5000, ANIMATION_DURATION);
    }

    public final double getAngleRange() {
        return null == angleRange ? _angleRange : angleRange.get();
    }
    public final void setAngleRange(final double ANGLE_RANGE) {
        if (null == angleRange) {
            _angleRange = clamp(0.0, 360.0, ANGLE_RANGE);
        } else {
            angleRange.set(ANGLE_RANGE);
        }
    }
    public final DoubleProperty angleRangeProperty() {
        if (null == angleRange) {
            angleRange = new DoublePropertyBase(_angleRange) {
                @Override protected void invalidated() { set(clamp(0d, 360d, get())); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "angleRange"; }
            };
        }
        return angleRange;
    }

    public final boolean isClockwise() {
        return null == clockwise ? _clockwise : clockwise.get();
    }
    public final void setClockwise(final boolean CLOCKWISE) {
        if (null == clockwise) {
            _clockwise = CLOCKWISE;
        } else {
            clockwise.set(CLOCKWISE);
        }
    }
    public final BooleanProperty clockwiseProperty() {
        if (null == clockwise) {
            clockwise = new SimpleBooleanProperty(this, "clockwise", _clockwise);
        }
        return clockwise;
    }

    public final boolean isAutoScale() {
        return null == autoScale ? _autoScale : autoScale.get();
    }
    public final void setAutoScale(final boolean AUTO_SCALE) {
        if (AUTO_SCALE) {
            exactMinValue = getMinValue();
            exactMaxValue = getMaxValue();
        } else {
            setMinValue(exactMinValue);
            setMaxValue(exactMaxValue);
        }
        if (null == autoScale) {
            _autoScale = AUTO_SCALE;
        } else {
            autoScale.set(AUTO_SCALE);
        }
    }
    public final BooleanProperty autoScaleProperty() {
        if (null == autoScale) {
            autoScale = new SimpleBooleanProperty(this, "autoScale", _autoScale);
        }
        return autoScale;
    }

    // Properties related to visualization
    public final Color getNeedleColor() {
        return null == needleColor ? _needleColor : needleColor.get();
    }
    public final void setNeedleColor(final Color NEEDLE_COLOR) {
        if (null == needleColor) {
            _needleColor = NEEDLE_COLOR;
        } else {
            needleColor.set(NEEDLE_COLOR);
        }
    }
    public final ObjectProperty<Color> needleColorProperty() {
        if (null == needleColor) {
            needleColor = new SimpleObjectProperty<>(this, "needleColor", _needleColor);
        }
        return needleColor;
    }

    public final ObservableList<Section> getSections() {
        return sections;
    }
    public final void setSections(final List<Section> SECTIONS) {
        sections.setAll(SECTIONS);
    }
    public final void setSections(final Section... SECTIONS) {
        setSections(Arrays.asList(SECTIONS));
    }
    public final void addSection(final Section SECTION) {
        if (!sections.contains(SECTION)) sections.add(SECTION);
    }
    public final void removeSection(final Section SECTION) {
        if (sections.contains(SECTION)) sections.remove(SECTION);
    }

    public final double getMajorTickSpace() {
        return null == majorTickSpace ? _majorTickSpace : majorTickSpace.get();
    }
    public final void setMajorTickSpace(final double MAJOR_TICK_SPACE) {
        if (null == majorTickSpace) {
            _majorTickSpace = MAJOR_TICK_SPACE;
        } else {
            majorTickSpace.set(MAJOR_TICK_SPACE);
        }
    }
    public final DoubleProperty majorTickSpaceProperty() {
        if (null == majorTickSpace) {
            majorTickSpace = new SimpleDoubleProperty(this, "majorTickSpace", _majorTickSpace);
        }
        return majorTickSpace;
    }

    public final double getMinorTickSpace() {
        return null == minorTickSpace ? _minorTickSpace : minorTickSpace.get();
    }
    public final void setMinorTickSpace(final double MINOR_TICK_SPACE) {
        if (null == minorTickSpace) {
            _minorTickSpace = MINOR_TICK_SPACE;
        } else {
            minorTickSpace.set(MINOR_TICK_SPACE);
        }
    }
    public final DoubleProperty minorTickSpaceProperty() {
        if (null == minorTickSpace) {
            minorTickSpace = new SimpleDoubleProperty(this, "minorTickSpace", _minorTickSpace);
        }
        return minorTickSpace;
    }

    public final String getTitle() {
        return null == title ? _title : title.get();
    }
    public final void setTitle(final String TITLE) {
        if (null == title) {
            _title = TITLE;
        } else {
            title.set(TITLE);
        }
    }
    public final StringProperty titleProperty() {
        if (null == title) {
            title = new SimpleStringProperty(this, "title", _title);
        }
        return title;
    }

    public final boolean isSectionTextVisible() {
        return null == sectionTextVisible ? _sectionTextVisible : sectionTextVisible.get();
    }
    public final void setSectionTextVisible(final boolean SECTION_TEXT_VISIBLE) {
        if (null == sectionTextVisible) {
            _sectionTextVisible = SECTION_TEXT_VISIBLE;
        } else {
            sectionTextVisible.set(SECTION_TEXT_VISIBLE);
        }
    }
    public final BooleanProperty sectionTextVisibleProperty() {
        if (null == sectionTextVisible) {
            sectionTextVisible = new SimpleBooleanProperty(this, "sectionTextVisible", _sectionTextVisible);
        }
        return sectionTextVisible;
    }

    public final boolean isSectionIconVisible() {
        return null == sectionIconVisible ? _sectionIconVisible : sectionIconVisible.get();
    }
    public final void setSectionIconVisible(final boolean SECTION_ICON_VISIBLE) {
        if (null == sectionIconVisible) {
            _sectionIconVisible = SECTION_ICON_VISIBLE;
        } else {
            sectionIconVisible.set(SECTION_ICON_VISIBLE);
        }
    }
    public final BooleanProperty sectionIconVisibleProperty() {
        if (null == sectionIconVisible) {
            sectionIconVisible = new SimpleBooleanProperty(this, "sectionIconVisible", _sectionIconVisible);
        }
        return sectionIconVisible;
    }

    public final boolean isMeasuredRangeVisible() {
        return null == measuredRangeVisible ? _measuredRangeVisible : measuredRangeVisible.get();
    }
    public final void setMeasuredRangeVisible(final boolean MEASURED_RANGE_VISIBLE) {
        if (null == measuredRangeVisible) {
            _measuredRangeVisible = MEASURED_RANGE_VISIBLE;
        } else {
            measuredRangeVisible.set(MEASURED_RANGE_VISIBLE);
        }
    }
    public final ReadOnlyBooleanProperty measuredRangeVisibleProperty() {
        if (null == measuredRangeVisible) {
            measuredRangeVisible = new SimpleBooleanProperty(this, "measuredRangeVisible", _measuredRangeVisible);
        }
        return measuredRangeVisible;
    }

    public final Color getValueTextColor() {
        return null == valueTextColor ? DEFAULT_VALUE_TEXT_COLOR : valueTextColor.get();
    }
    public final void setValueTextColor(Color value) {
        valueTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> valueTextColorProperty() {
        if (null == valueTextColor) {
            valueTextColor = new SimpleObjectProperty<>(this, "valueTextColor", DEFAULT_VALUE_TEXT_COLOR);
        }
        return valueTextColor;
    }

    public final Color getTitleTextColor() {
        return null == titleTextColor ? DEFAULT_TITLE_TEXT_COLOR : titleTextColor.get();
    }
    public final void setTitleTextColor(Color value) {
        titleTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> titleTextColorProperty() {
        if (null == titleTextColor) {
            titleTextColor = new SimpleObjectProperty<>(this, "titleTextColor", DEFAULT_TITLE_TEXT_COLOR);
        }
        return titleTextColor;
    }

    public final Color getSectionTextColor() {
        return null == sectionTextColor ? DEFAULT_SECTION_TEXT_COLOR : sectionTextColor.get();
    }
    public final void setSectionTextColor(Color value) {
        sectionTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> sectionTextColorProperty() {
        if (null == sectionTextColor) {
            sectionTextColor = new SimpleObjectProperty<>(this, "sectionTextColor", DEFAULT_SECTION_TEXT_COLOR);
        }
        return sectionTextColor;
    }

    public final Color getRangeFill() {
        return null == rangeFill ? DEFAULT_RANGE_FILL : rangeFill.get();
    }
    public final void setRangeFill(Color value) {
        rangeFillProperty().set(value);
    }
    public final ObjectProperty<Color> rangeFillProperty() {
        if (null == rangeFill) {
            rangeFill = new SimpleObjectProperty<>(this, "rangeFill", DEFAULT_RANGE_FILL);
        }
        return rangeFill;
    }
    
    private double clamp(final double MIN_VALUE, final double MAX_VALUE, final double VALUE) {
        if (VALUE < MIN_VALUE) return MIN_VALUE;
        if (VALUE > MAX_VALUE) return MAX_VALUE;
        return VALUE;
    }
    private int clamp(final int MIN_VALUE, final int MAX_VALUE, final int VALUE) {
        if (VALUE < MIN_VALUE) return MIN_VALUE;
        if (VALUE > MAX_VALUE) return MAX_VALUE;
        return VALUE;
    }
    private Duration clamp(final Duration MIN_VALUE, final Duration MAX_VALUE, final Duration VALUE) {
        if (VALUE.lessThan(MIN_VALUE)) return MIN_VALUE;
        if (VALUE.greaterThan(MAX_VALUE)) return MAX_VALUE;
        return VALUE;
    }

    public void calcAutoScale() {
        if (isAutoScale()) {
            double maxNoOfMajorTicks = 10;
            double maxNoOfMinorTicks = 10;
            double niceMinValue;
            double niceMaxValue;
            double niceRange;
            niceRange = (calcNiceNumber((getMaxValue() - getMinValue()), false));
            majorTickSpace.set(calcNiceNumber(niceRange / (maxNoOfMajorTicks - 1), true));
            niceMinValue = (Math.floor(getMinValue() / majorTickSpace.doubleValue()) * majorTickSpace.doubleValue());
            niceMaxValue = (Math.ceil(getMaxValue() / majorTickSpace.doubleValue()) * majorTickSpace.doubleValue());
            minorTickSpace.set(calcNiceNumber(majorTickSpace.doubleValue() / (maxNoOfMinorTicks - 1), true));
            setMinValue(niceMinValue);
            setMaxValue(niceMaxValue);
        }
    }

    /**
     * Returns a "niceScaling" number approximately equal to the range.
     * Rounds the number if ROUND == true.
     * Takes the ceiling if ROUND = false.
     *
     * @param RANGE the value range (maxValue - minValue)
     * @param ROUND whether to round the result or ceil
     * @return a "niceScaling" number to be used for the value range
     */
    private double calcNiceNumber(final double RANGE, final boolean ROUND) {
        final double EXPONENT = Math.floor(Math.log10(RANGE));   // exponent of range
        final double FRACTION = RANGE / Math.pow(10, EXPONENT);  // fractional part of range
        //final double MOD      = FRACTION % 0.5;                  // needed for large number scale
        double niceFraction;

        // niceScaling
        /*
        if (isLargeNumberScale()) {
            if (MOD != 0) {
                niceFraction = FRACTION - MOD;
                niceFraction += 0.5;
            } else {
                niceFraction = FRACTION;
            }
        } else {
        */

        if (ROUND) {
            if (FRACTION < 1.5) {
                niceFraction = 1;
            } else if (FRACTION < 3) {
                niceFraction = 2;
            } else if (FRACTION < 7) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        } else {
            if (Double.compare(FRACTION, 1) <= 0) {
                niceFraction = 1;
            } else if (Double.compare(FRACTION, 2) <= 0) {
                niceFraction = 2;
            } else if (Double.compare(FRACTION, 5) <= 0) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        }
        //}
        return niceFraction * Math.pow(10, EXPONENT);
    }


    // ******************** Private Methods ***********************************
    private void rotateNeedle() {
        angleStep          = getAngleRange() / (getMaxValue() - getMinValue());
        double targetAngle = needleRotate.getAngle() + (getValue() - getOldValue()) * angleStep;
        targetAngle        = clamp(180 - getStartAngle(), 180 - getStartAngle() + getAngleRange(), targetAngle);

        if (isAnimated()) {
            timeline.stop();
            final KeyValue KEY_VALUE = new KeyValue(needleRotate.angleProperty(), targetAngle, Interpolator.SPLINE(0.5,
                                                                                                                   0.4,
                                                                                                                   0.4,
                                                                                                                   1.0));
            final KeyFrame KEY_FRAME = new KeyFrame(Duration.millis(getAnimationDuration()), KEY_VALUE);
            timeline.getKeyFrames().setAll(KEY_FRAME);
            timeline.play();
        } else {
            needleRotate.setAngle(targetAngle);
        }
    }

    private final void drawSections() {
        sectionsCtx.clearRect(0, 0, size, size);
        final double MIN_VALUE       = getMinValue();
        final double MAX_VALUE       = getMaxValue();
        final double OFFSET          = getStartAngle() - 90;
        final int NO_OF_SECTIONS     = getSections().size();
        final double SECTIONS_OFFSET = size * 0.015;
        final double SECTIONS_SIZE   = size - (size * 0.03);
        double sinValue;
        double cosValue;
        for (int i = 0 ; i < NO_OF_SECTIONS ; i++) {
            final Section SECTION = getSections().get(i);
            final double SECTION_START_ANGLE;
            if (SECTION.getStart() > MAX_VALUE || SECTION.getStop() < MIN_VALUE) continue;

            if (SECTION.getStart() < MIN_VALUE && SECTION.getStop() < MAX_VALUE) {
                SECTION_START_ANGLE = MIN_VALUE * angleStep;
            } else {
                SECTION_START_ANGLE = (SECTION.getStart() - MIN_VALUE) * angleStep;
            }
            final double SECTION_ANGLE_EXTEND;
            if (SECTION.getStop() > MAX_VALUE) {
                SECTION_ANGLE_EXTEND = MAX_VALUE * angleStep;
            } else {
                SECTION_ANGLE_EXTEND = (SECTION.getStop() - SECTION.getStart()) * angleStep;
            }
            sectionsCtx.save();
            sectionsCtx.setFill(SECTION.getColor());
            sectionsCtx.fillArc(SECTIONS_OFFSET, SECTIONS_OFFSET, SECTIONS_SIZE, SECTIONS_SIZE, (OFFSET - SECTION_START_ANGLE), -SECTION_ANGLE_EXTEND, ArcType.ROUND);

            // Draw Section Text
            if (isSectionTextVisible()) {
                sinValue = -Math.sin(Math.toRadians(OFFSET - 90 - SECTION_START_ANGLE - SECTION_ANGLE_EXTEND * 0.5));
                cosValue = -Math.cos(Math.toRadians(OFFSET - 90 - SECTION_START_ANGLE - SECTION_ANGLE_EXTEND * 0.5));
                Point2D textPoint = new Point2D(size * 0.5 + size * 0.365 * sinValue, size * 0.5 + size * 0.365 * cosValue);
                sectionsCtx.setFont(Font.font("Open Sans", FontWeight.NORMAL, 0.08 * size));
                sectionsCtx.setTextAlign(TextAlignment.CENTER);
                sectionsCtx.setTextBaseline(VPos.CENTER);
                sectionsCtx.setFill(getSectionTextColor());
                sectionsCtx.fillText(SECTION.getText(), textPoint.getX(), textPoint.getY());
            }

            // Draw Section Icon
            if (size > 0) {
                if (isSectionIconVisible() && !isSectionTextVisible()) {
                    if (null != SECTION.getImage()) {
                        Image icon = SECTION.getImage();
                        sinValue = -Math.sin(Math.toRadians(OFFSET - 90 - SECTION_START_ANGLE - SECTION_ANGLE_EXTEND * 0.5));
                        cosValue = -Math.cos(Math.toRadians(OFFSET - 90 - SECTION_START_ANGLE - SECTION_ANGLE_EXTEND * 0.5));
                        Point2D iconPoint = new Point2D(size * 0.5 + size * 0.365 * sinValue, size * 0.5 + size * 0.365 * cosValue);
                        sectionsCtx.drawImage(icon, iconPoint.getX() - size * 0.06, iconPoint.getY() - size * 0.06, size * 0.12, size * 0.12);
                    }
                }
            }
            sectionsCtx.restore();

            // Draw white border around area                        
            sectionsCtx.setStroke(Color.WHITE);
            sectionsCtx.setLineWidth(size * 0.032);
            sectionsCtx.strokeArc(SECTIONS_OFFSET, SECTIONS_OFFSET, SECTIONS_SIZE, SECTIONS_SIZE, OFFSET + 90, 270, ArcType.ROUND);
        }
    }

    private final void drawMeasuredRange() {
        final double MIN_VALUE    = getMinValue();
        final double OFFSET       = getStartAngle() - 90;
        final double START_ANGLE  = (getMinMeasuredValue() - MIN_VALUE) * angleStep;
        final double ANGLE_EXTEND = (getMaxMeasuredValue() - getMinMeasuredValue()) * angleStep;
        final double RANGE_OFFSET = size * 0.015;
        final double RANGE_SIZE   = size - (size * 0.03);

        measuredRangeCtx.save();
        measuredRangeCtx.clearRect(0, 0, size, size);
        measuredRangeCtx.setFill(getRangeFill());
        measuredRangeCtx.fillArc(RANGE_OFFSET, RANGE_OFFSET, RANGE_SIZE, RANGE_SIZE, (OFFSET - START_ANGLE), -ANGLE_EXTEND, ArcType.ROUND);
        measuredRangeCtx.setStroke(Color.WHITE);
        measuredRangeCtx.setLineWidth(size * 0.032);
        measuredRangeCtx.strokeArc(RANGE_OFFSET, RANGE_OFFSET, RANGE_SIZE, RANGE_SIZE, (OFFSET - START_ANGLE), -ANGLE_EXTEND, ArcType.ROUND);
        measuredRangeCtx.restore();
    }
    
    private void resizeText() {
        valueText.setFont(Font.font("Open Sans", FontWeight.BOLD, size * 0.145));
        if (valueText.getLayoutBounds().getWidth() > 0.38 * size) {
            double decrement = 0d;
            while (valueText.getLayoutBounds().getWidth() > 0.38 * size && valueText.getFont().getSize() > 0) {
                valueText.setFont(Font.font("Open Sans", FontWeight.BOLD, size * (0.15 - decrement)));
                decrement += 0.01;
            }
        }
        valueText.setTranslateX((size - valueText.getLayoutBounds().getWidth()) * 0.5);
        valueText.setTranslateY(size * (titleText.getText().isEmpty() ? 0.5 : 0.48));

        titleText.setFont(Font.font("Open Sans", FontWeight.BOLD, size * 0.045));
        if (titleText.getLayoutBounds().getWidth() > 0.38 * size) {
            double decrement = 0d;
            while (titleText.getLayoutBounds().getWidth() > 0.38 * size && titleText.getFont().getSize() > 0) {
                titleText.setFont(Font.font("Open Sans", FontWeight.BOLD, size * (0.05 - decrement)));
                decrement += 0.01;
            }
        }
        titleText.setTranslateX((size - titleText.getLayoutBounds().getWidth()) * 0.5);
        titleText.setTranslateY(size * 0.5 + valueText.getFont().getSize() * 0.7);
    }

    private void resize() {
        size = getWidth() < getHeight() ? getWidth() : getHeight();

        sectionsCanvas.setWidth(size);
        sectionsCanvas.setHeight(size);
        drawSections();

        measuredRangeCanvas.setWidth(size);
        measuredRangeCanvas.setHeight(size);
        drawMeasuredRange();

        double currentValue = (needleRotate.getAngle() + getStartAngle() - 180) / angleStep + getMinValue();
        valueText.setText(String.format(Locale.US, "%." + getDecimals() + "f", currentValue) + getUnit());
        //valueText.setText(String.format(Locale.US, "%." + getDecimals() + "f", (needleRotate.getAngle() + getStartAngle() - 180) / angleStep) + getUnit());

        titleText.setText(getTitle());

        needle.getElements().clear();
        needle.getElements().add(new MoveTo(0.275 * size, 0.5 * size));
        needle.getElements().add(new CubicCurveTo(0.275 * size, 0.62426575 * size,
                                                  0.37573425 * size, 0.725 * size,
                                                  0.5 * size, 0.725 * size));
        needle.getElements().add(new CubicCurveTo(0.62426575 * size, 0.725 * size,
                                                  0.725 * size, 0.62426575 * size,
                                                  0.725 * size, 0.5 * size));
        needle.getElements().add(new CubicCurveTo(0.725 * size, 0.3891265 * size,
                                                  0.6448105 * size, 0.296985 * size,
                                                  0.5392625 * size, 0.2784125 * size));
        needle.getElements().add(new LineTo(0.5 * size, 0.0225));
        needle.getElements().add(new LineTo(0.4607375 * size, 0.2784125 * size));
        needle.getElements().add(new CubicCurveTo(0.3551895 * size, 0.296985 * size,
                                                  0.275 * size, 0.3891265 * size,
                                                  0.275 * size, 0.5 * size));
        needle.getElements().add(new ClosePath());
        needle.setStrokeWidth(size * 0.03);
        needle.setFill(getNeedleColor());

        needle.relocate(needle.getLayoutBounds().getMinX(), needle.getLayoutBounds().getMinY());
        needleRotate.setPivotX(size * 0.5);
        needleRotate.setPivotY(size * 0.5);

        resizeText();
    }
}
