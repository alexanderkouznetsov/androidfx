/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.gauge;

import eu.hansolo.enzo.common.Section;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.IntegerPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.Arrays;
import java.util.List;


/**
 * Created by
 * User: hansolo
 * Date: 01.04.13
 * Time: 17:10
 */
public class SimpleGauge extends Control {    
    
    // Control related    
    private static final Color DEFAULT_VALUE_TEXT_COLOR   = Color.web("#ffffff");
    private static final Color DEFAULT_TITLE_TEXT_COLOR   = Color.web("#ffffff");
    private static final Color DEFAULT_SECTION_TEXT_COLOR = Color.web("#ffffff");        
    private static final Color DEFAULT_RANGE_FILL = Color.rgb(0, 0, 0, 0.25);
    private DoubleProperty  value;
    private double          oldValue;
    private DoubleProperty  minValue;
    private double          exactMinValue;
    private DoubleProperty  maxValue;
    private double          exactMaxValue;
    private double          minMeasuredValue;
    private double          maxMeasuredValue;
    private int             _decimals;
    private IntegerProperty decimals;
    private String          _unit;
    private StringProperty  unit;
    private boolean         _animated;
    private BooleanProperty animated;
    private double          animationDuration;
    private double          _startAngle;
    private DoubleProperty  startAngle;
    private double          _angleRange;
    private DoubleProperty  angleRange;
    private boolean         _clockwise;
    private BooleanProperty clockwise;
    private boolean         _autoScale;
    private BooleanProperty autoScale;
    private boolean         _sectionTextVisible;
    private BooleanProperty sectionTextVisible;
    private boolean         _sectionIconVisible;
    private BooleanProperty sectionIconVisible;
    private boolean         _measuredRangeVisible;
    private BooleanProperty measuredRangeVisible;
    private Color                   _needleColor;
    private ObjectProperty<Color>   needleColor;
    private ObservableList<Section> sections;
    private double                  _majorTickSpace;
    private DoubleProperty          majorTickSpace;
    private double                  _minorTickSpace;
    private DoubleProperty          minorTickSpace;
    private String                  _title;
    private StringProperty          title;
    private ObjectProperty<Color>       valueTextColor;
    private ObjectProperty<Color>       titleTextColor;
    private ObjectProperty<Color>       sectionTextColor;   
    private ObjectProperty<Color>       rangeFill;
    
    // Graphics related
    


    // ******************** Constructors **************************************
    public SimpleGauge() {
        getStyleClass().add("simple-gauge");
        value = new DoublePropertyBase(0) {
            @Override protected void invalidated() {                
                set(clamp(getMinValue(), getMaxValue(), get()));
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "value"; }
        };
        minValue = new DoublePropertyBase(0) {
            @Override protected void invalidated() {
                set(clamp(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, get()));
                if (getValue() < get()) setValue(get());
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "minValue"; }
        };
        maxValue = new DoublePropertyBase(100) {
            @Override protected void invalidated() {
                set(clamp(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, get()));
                if (getValue() > get()) setValue(get());
            }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "maxValue"; }
        };
        oldValue = 0;
        _decimals = 0;
        _unit = "";
        _animated = true;
        _startAngle = 315;
        _angleRange = 270;
        _clockwise = true;
        _autoScale = false;
        _needleColor = Color.web("#5a615f");
        _sectionTextVisible = false;
        _sectionIconVisible = false;
        _measuredRangeVisible = false;
        sections = FXCollections.observableArrayList();
        _majorTickSpace = 10;
        _minorTickSpace = 1;
        _title = "";
        animationDuration     = 3000; 
    }


    // ******************** Methods *******************************************
    public final double getValue() {
        return value.get();
    }
    public final void setValue(final double VALUE) {
        oldValue = valueProperty().get();        
        value.set(VALUE);
    }
    public final DoubleProperty valueProperty() {                        
        return value;
    }

    public final double getOldValue() {
        return oldValue;
    }

    public final double getMinValue() {
        return minValue.get();
    }
    public final void setMinValue(final double MIN_VALUE) {
        minValue.set(MIN_VALUE);
    }
    public final DoubleProperty minValueProperty() {                
        return minValue;
    }

    public final double getMaxValue() {
        return maxValue.get();
    }
    public final void setMaxValue(final double MAX_VALUE) {
        maxValue.set(MAX_VALUE);
    }
    public final DoubleProperty maxValueProperty() {
        return maxValue;
    }

    public final double getMinMeasuredValue() {
        return minMeasuredValue;
    }
    public final void setMinMeasuredValue(final double MIN_MEASURED_VALUE) {
        minMeasuredValue = clamp(getMinValue(), getMaxValue(), MIN_MEASURED_VALUE);
    }
    
    public final double getMaxMeasuredValue() {
        return maxMeasuredValue;
    } 
    public final void setMaxMeasuredValue(final double MAX_MEASURED_VALUE) {
        maxMeasuredValue = clamp(getMinValue(), getMaxValue(), MAX_MEASURED_VALUE);
    }
    
    public final void resetMinMaxMeasuredValues() {
        minMeasuredValue = getValue();
        maxMeasuredValue = getValue();
    }
    
    public final int getDecimals() {
        return null == decimals ? _decimals : decimals.get();
    }
    public final void setDecimals(final int DECIMALS) {
        if (null == decimals) {
            _decimals = clamp(0, 3, DECIMALS);
        } else {
            decimals.set(DECIMALS);
        }
    }
    public final IntegerProperty decimalsProperty() {
        if (null == decimals) {
            decimals  = new IntegerPropertyBase(_decimals) {
                @Override protected void invalidated() { set(clamp(0, 3, get())); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "decimals"; }
            };
        }
        return decimals;
    }

    public final String getUnit() {
        return null == unit ? _unit : unit.get();
    }
    public final void setUnit(final String UNIT) {
        if (null == unit) {
            _unit = UNIT;
        } else {
            unit.set(UNIT);
        }
    }
    public final StringProperty unitProperty() {
        if (null == unit) {
            unit = new SimpleStringProperty(this, "unit", _unit);
        }
        return unit;
    }

    public final boolean isAnimated() {
        return null == animated ? _animated : animated.get();
    }
    public final void setAnimated(final boolean ANIMATED) {
        if (null == animated) {
            _animated = ANIMATED;
        } else {
            animated.set(ANIMATED);
        }
    }
    public final BooleanProperty animatedProperty() {
        if (null == animated) {
            animated = new SimpleBooleanProperty(this, "animated", _animated);
        }
        return animated;
    }

    public double getStartAngle() {
        return null == startAngle ? _startAngle : startAngle.get();
    }
    public final void setStartAngle(final double START_ANGLE) {
        if (null == startAngle) {
            _startAngle = clamp(0, 360, START_ANGLE);
        } else {
            startAngle.set(START_ANGLE);
        }
    }
    public final DoubleProperty startAngleProperty() {
        if (null == startAngle) {            
            startAngle = new DoublePropertyBase(_startAngle) {
                @Override protected void invalidated() {
                    set(clamp(0d, 360d, get()));                    
                }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "startAngle"; }
            };
        }
        return startAngle;
    }

    public final double getAnimationDuration() {
        return animationDuration;
    }
    public final void setAnimationDuration(final double ANIMATION_DURATION) {
        animationDuration = clamp(20, 5000, ANIMATION_DURATION);
    }

    public final double getAngleRange() {
        return null == angleRange ? _angleRange : angleRange.get();
    }
    public final void setAngleRange(final double ANGLE_RANGE) {
        if (null == angleRange) {
            _angleRange = clamp(0.0, 360.0, ANGLE_RANGE);
        } else {
            angleRange.set(ANGLE_RANGE);
        }
    }
    public final DoubleProperty angleRangeProperty() {
        if (null == angleRange) {                        
            angleRange = new DoublePropertyBase(_angleRange) {
                @Override protected void invalidated() { set(clamp(0d, 360d, get())); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "angleRange"; }
            };
        }
        return angleRange;
    }

    public final boolean isClockwise() {
        return null == clockwise ? _clockwise : clockwise.get();
    }
    public final void setClockwise(final boolean CLOCKWISE) {
        if (null == clockwise) {
            _clockwise = CLOCKWISE;
        } else {
            clockwise.set(CLOCKWISE);
        }
    }
    public final BooleanProperty clockwiseProperty() {
        if (null == clockwise) {
            clockwise = new SimpleBooleanProperty(this, "clockwise", _clockwise);
        }
        return clockwise;
    }

    public final boolean isAutoScale() {
        return null == autoScale ? _autoScale : autoScale.get();
    }
    public final void setAutoScale(final boolean AUTO_SCALE) {
        if (AUTO_SCALE) {
            exactMinValue = getMinValue();
            exactMaxValue = getMaxValue();
        } else {
            setMinValue(exactMinValue);
            setMaxValue(exactMaxValue);
        }
        if (null == autoScale) {
            _autoScale = AUTO_SCALE;
        } else {
            autoScale.set(AUTO_SCALE);
        }
    }
    public final BooleanProperty autoScaleProperty() {
        if (null == autoScale) {
            autoScale = new SimpleBooleanProperty(this, "autoScale", _autoScale);
        }
        return autoScale;
    }

    // Properties related to visualization
    public final Color getNeedleColor() {
        return null == needleColor ? _needleColor : needleColor.get();
    }
    public final void setNeedleColor(final Color NEEDLE_COLOR) {
        if (null == needleColor) {
            _needleColor = NEEDLE_COLOR;
        } else {
            needleColor.set(NEEDLE_COLOR);
        }
    }
    public final ObjectProperty<Color> needleColorProperty() {
        if (null == needleColor) {
            needleColor = new SimpleObjectProperty<>(this, "needleColor", _needleColor);
        }
        return needleColor;
    }

    public final ObservableList<Section> getSections() {
        return sections;
    }
    public final void setSections(final List<Section> SECTIONS) {
        sections.setAll(SECTIONS);
    }
    public final void setSections(final Section... SECTIONS) {
        setSections(Arrays.asList(SECTIONS));
    }
    public final void addSection(final Section SECTION) {
        if (!sections.contains(SECTION)) sections.add(SECTION);
    }
    public final void removeSection(final Section SECTION) {
        if (sections.contains(SECTION)) sections.remove(SECTION);
    }

    public final double getMajorTickSpace() {
        return null == majorTickSpace ? _majorTickSpace : majorTickSpace.get();
    }
    public final void setMajorTickSpace(final double MAJOR_TICK_SPACE) {
        if (null == majorTickSpace) {
            _majorTickSpace = MAJOR_TICK_SPACE;
        } else {
            majorTickSpace.set(MAJOR_TICK_SPACE);
        }
    }
    public final DoubleProperty majorTickSpaceProperty() {
        if (null == majorTickSpace) {
            majorTickSpace = new SimpleDoubleProperty(this, "majorTickSpace", _majorTickSpace);
        }
        return majorTickSpace;
    }

    public final double getMinorTickSpace() {
        return null == minorTickSpace ? _minorTickSpace : minorTickSpace.get();
    }
    public final void setMinorTickSpace(final double MINOR_TICK_SPACE) {
        if (null == minorTickSpace) {
            _minorTickSpace = MINOR_TICK_SPACE;
        } else {
            minorTickSpace.set(MINOR_TICK_SPACE);
        }
    }
    public final DoubleProperty minorTickSpaceProperty() {
        if (null == minorTickSpace) {
            minorTickSpace = new SimpleDoubleProperty(this, "minorTickSpace", _minorTickSpace);
        }
        return minorTickSpace;
    }

    public final String getTitle() {
        return null == title ? _title : title.get();
    }
    public final void setTitle(final String TITLE) {
        if (null == title) {
            _title = TITLE;
        } else {
            title.set(TITLE);
        }
    }
    public final StringProperty titleProperty() {
        if (null == title) {
            title = new SimpleStringProperty(this, "title", _title);
        }
        return title;
    }

    public final boolean isSectionTextVisible() {
        return null == sectionTextVisible ? _sectionTextVisible : sectionTextVisible.get();
    }
    public final void setSectionTextVisible(final boolean SECTION_TEXT_VISIBLE) {
        if (null == sectionTextVisible) {
            _sectionTextVisible = SECTION_TEXT_VISIBLE;
        } else {
            sectionTextVisible.set(SECTION_TEXT_VISIBLE);
        }
    }
    public final BooleanProperty sectionTextVisibleProperty() {
        if (null == sectionTextVisible) {
            sectionTextVisible = new SimpleBooleanProperty(this, "sectionTextVisible", _sectionTextVisible);
        }
        return sectionTextVisible;
    }

    public final boolean isSectionIconVisible() {
        return null == sectionIconVisible ? _sectionIconVisible : sectionIconVisible.get();
    }
    public final void setSectionIconVisible(final boolean SECTION_ICON_VISIBLE) {
        if (null == sectionIconVisible) {
            _sectionIconVisible = SECTION_ICON_VISIBLE;
        } else {
            sectionIconVisible.set(SECTION_ICON_VISIBLE);
        }
    }
    public final BooleanProperty sectionIconVisibleProperty() {
        if (null == sectionIconVisible) {
            sectionIconVisible = new SimpleBooleanProperty(this, "sectionIconVisible", _sectionIconVisible);
        }
        return sectionIconVisible;
    }
    
    public final boolean isMeasuredRangeVisible() {
        return null == measuredRangeVisible ? _measuredRangeVisible : measuredRangeVisible.get();
    }
    public final void setMeasuredRangeVisible(final boolean MEASURED_RANGE_VISIBLE) {
        if (null == measuredRangeVisible) {
            _measuredRangeVisible = MEASURED_RANGE_VISIBLE;
        } else {
            measuredRangeVisible.set(MEASURED_RANGE_VISIBLE);
        }
    }
    public final ReadOnlyBooleanProperty measuredRangeVisibleProperty() {
        if (null == measuredRangeVisible) {
            measuredRangeVisible = new SimpleBooleanProperty(this, "measuredRangeVisible", _measuredRangeVisible);
        }
        return measuredRangeVisible;
    }
            
    private double clamp(final double MIN_VALUE, final double MAX_VALUE, final double VALUE) {
        if (VALUE < MIN_VALUE) return MIN_VALUE;
        if (VALUE > MAX_VALUE) return MAX_VALUE;
        return VALUE;
    }
    private int clamp(final int MIN_VALUE, final int MAX_VALUE, final int VALUE) {
        if (VALUE < MIN_VALUE) return MIN_VALUE;
        if (VALUE > MAX_VALUE) return MAX_VALUE;
        return VALUE;
    }
    private Duration clamp(final Duration MIN_VALUE, final Duration MAX_VALUE, final Duration VALUE) {
        if (VALUE.lessThan(MIN_VALUE)) return MIN_VALUE;
        if (VALUE.greaterThan(MAX_VALUE)) return MAX_VALUE;
        return VALUE;
    }

    public void calcAutoScale() {
        if (isAutoScale()) {
            double maxNoOfMajorTicks = 10;
            double maxNoOfMinorTicks = 10;
            double niceMinValue;
            double niceMaxValue;
            double niceRange;
            niceRange = (calcNiceNumber((getMaxValue() - getMinValue()), false));
            majorTickSpace.set(calcNiceNumber(niceRange / (maxNoOfMajorTicks - 1), true));
            niceMinValue = (Math.floor(getMinValue() / majorTickSpace.doubleValue()) * majorTickSpace.doubleValue());
            niceMaxValue = (Math.ceil(getMaxValue() / majorTickSpace.doubleValue()) * majorTickSpace.doubleValue());
            minorTickSpace.set(calcNiceNumber(majorTickSpace.doubleValue() / (maxNoOfMinorTicks - 1), true));
            setMinValue(niceMinValue);
            setMaxValue(niceMaxValue);
        }
    }

    /**
     * Returns a "niceScaling" number approximately equal to the range.
     * Rounds the number if ROUND == true.
     * Takes the ceiling if ROUND = false.
     *
     * @param RANGE the value range (maxValue - minValue)
     * @param ROUND whether to round the result or ceil
     * @return a "niceScaling" number to be used for the value range
     */
    private double calcNiceNumber(final double RANGE, final boolean ROUND) {
        final double EXPONENT = Math.floor(Math.log10(RANGE));   // exponent of range
        final double FRACTION = RANGE / Math.pow(10, EXPONENT);  // fractional part of range
        //final double MOD      = FRACTION % 0.5;                  // needed for large number scale
        double niceFraction;

        // niceScaling
        /*
        if (isLargeNumberScale()) {
            if (MOD != 0) {
                niceFraction = FRACTION - MOD;
                niceFraction += 0.5;
            } else {
                niceFraction = FRACTION;
            }
        } else {
        */

        if (ROUND) {
            if (FRACTION < 1.5) {
                niceFraction = 1;
            } else if (FRACTION < 3) {
                niceFraction = 2;
            } else if (FRACTION < 7) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        } else {
            if (Double.compare(FRACTION, 1) <= 0) {
                niceFraction = 1;
            } else if (Double.compare(FRACTION, 2) <= 0) {
                niceFraction = 2;
            } else if (Double.compare(FRACTION, 5) <= 0) {
                niceFraction = 5;
            } else {
                niceFraction = 10;
            }
        }
        //}
        return niceFraction * Math.pow(10, EXPONENT);
    }
   

    // ******************** CSS Stylable Properties ***************************
    public final Color getValueTextColor() {
        return null == valueTextColor ? DEFAULT_VALUE_TEXT_COLOR : valueTextColor.get();
    }
    public final void setValueTextColor(Color value) {
        valueTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> valueTextColorProperty() {
        if (null == valueTextColor) {
            valueTextColor = new SimpleObjectProperty<>(this, "valueTextColor", DEFAULT_VALUE_TEXT_COLOR);
        }
        return valueTextColor;
    }

    public final Color getTitleTextColor() {
        return null == titleTextColor ? DEFAULT_TITLE_TEXT_COLOR : titleTextColor.get();
    }
    public final void setTitleTextColor(Color value) {
        titleTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> titleTextColorProperty() {
        if (null == titleTextColor) {
            titleTextColor = new SimpleObjectProperty<>(this, "titleTextColor", DEFAULT_TITLE_TEXT_COLOR);
        }
        return titleTextColor;
    }

    public final Color getSectionTextColor() {
        return null == sectionTextColor ? DEFAULT_SECTION_TEXT_COLOR : sectionTextColor.get();
    }
    public final void setSectionTextColor(Color value) {
        sectionTextColorProperty().set(value);
    }
    public final ObjectProperty<Color> sectionTextColorProperty() {
        if (null == sectionTextColor) {            
            sectionTextColor = new SimpleObjectProperty<>(this, "sectionTextColor", DEFAULT_SECTION_TEXT_COLOR);
        }
        return sectionTextColor;
    }
   
    public final Color getRangeFill() {
        return null == rangeFill ? DEFAULT_RANGE_FILL : rangeFill.get();
    }
    public final void setRangeFill(Color value) {
        rangeFillProperty().set(value);
    }
    public final ObjectProperty<Color> rangeFillProperty() {
        if (null == rangeFill) {            
            rangeFill = new SimpleObjectProperty<>(this, "rangeFill", DEFAULT_RANGE_FILL);
        }
        return rangeFill;
    }
    

    // ******************** Style related *************************************    
    @Override protected String getUserAgentStylesheet() {
        return SimpleGauge.class.getResource("simple-gauge.css").toExternalForm();
    }    
}
