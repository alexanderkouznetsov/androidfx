/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.gauge;

import eu.hansolo.enzo.common.Section;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Random;


/**
 * Created by
 * User: hansolo
 * Date: 22.07.13
 * Time: 08:41
 */

public class Demo extends Application {
    private static final Random RND = new Random();
    //private SimpleGauge    control;
    private Gauge          control;
    private long           lastTimerCall;
    private AnimationTimer timer;

    @Override public void init() {
        /*
        control = SimpleGaugeBuilder.create()
                                    .prefSize(400, 400)
                                    .sections(new Section(0, 16.66666, "0", Color.web("#20bafb")),
                                              new Section(16.66666, 33.33333, "1", Color.web("#23d8a6")),
                                              new Section(33.33333, 50.0, "2", Color.web("29fa3d")),
                                              new Section(50.0, 66.66666, "3", Color.web("#ebfb37")),
                                              new Section(66.66666, 83.33333, "4", Color.web("#fdb42a")),
                                              new Section(83.33333, 100.0, "5", Color.web("#fb4a1c")))
                                    .title("Temperature")
                                    .unit("C")
                                    .value(20)
                                    .build();
        */
        control = new Gauge();
        control.setPrefSize(400, 400);
        control.setSections(new Section[] {
                                           new Section(0, 16.66666, "0", Color.web("#20bafb")),
                                           new Section(16.66666, 33.33333, "1", Color.web("#23d8a6")),
                                           new Section(33.33333, 50.0, "2", Color.web("29fa3d")),
                                           new Section(50.0, 66.66666, "3", Color.web("#ebfb37")),
                                           new Section(66.66666, 83.33333, "4", Color.web("#fdb42a")),
                                           new Section(83.33333, 100.0, "5", Color.web("#fb4a1c"))});
        control.setTitle("Temperature");
        control.setUnit("C");
        control.setValue(20);
        lastTimerCall = System.nanoTime() + 2_000_000_000l;
        timer = new AnimationTimer() {
            @Override public void handle(long now) {
                if (now > lastTimerCall + 5_000_000_000l) {
                    control.setValue(RND.nextDouble() * 100);                    
                    lastTimerCall = now;
                }
            }
        };
    }

    @Override public void start(Stage stage) throws Exception {
        HBox pane = new HBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setSpacing(10);
        pane.getChildren().addAll(control);

        final Scene scene = new Scene(pane, 400, 400, Color.BLACK);
        //scene.setFullScreen(true);

        stage.setScene(scene);
        stage.show();
       
        timer.start();        
    }

    @Override public void stop() {

    }

    public static void main(final String[] args) {
        Application.launch(args);
    }    
}
